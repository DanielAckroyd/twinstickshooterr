﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimTestScript : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        
    }

    public float speed;

    // Update is called once per frame
    void Update()
    {
        var mousePos = Input.mousePosition;
        var camRay = Camera.main.ScreenPointToRay(mousePos);
        var playField = new Plane(Vector3.back, Vector3.zero);

        float outDist = 0;
        if (playField.Raycast(camRay, out outDist))
        {
            var hitPos = camRay.GetPoint(outDist);

            Debug.DrawLine(transform.position, hitPos, Color.red);

            var dif = hitPos - transform.position;
            Quaternion rot = Quaternion.LookRotation(transform.position - hitPos, Vector3.forward);
            transform.rotation = rot;


            //give that to a new bullet as which way it needs to travel
            var bulletDirection = dif.normalized;
        }
        
        float forwardinput = Input.GetAxis("Vertical") * -speed;
        float sideinput = Input.GetAxis("Horizontal") * speed;
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * forwardinput);
        rb.AddForce(transform.right * sideinput);


    }
}
